# Pull docker image of php 7.2
FROM php:7.2

# Unlock php banned-list & allow additional php module
RUN rm /etc/apt/preferences.d/no-debian-php

# Allow services from starting automatically
RUN echo "exit 0" > /usr/sbin/policy-rc.d

# Install other depencies (nginx, mysql)
RUN apt update && apt install nginx -y
RUN apt install net-tools mariadb-server php-mysql -y
RUN apt install php-fpm -y 

# Set working directory
WORKDIR /var/www/html

# Copy source code
COPY --chown=www-data:www-data laravel /var/www/html
COPY ./init-db.sh /init-db.sh

COPY ./config/default.conf /etc/nginx/sites-enabled/default
COPY ./config/www.conf /etc/php/7.3/fpm/pool.d/www.conf

# Set environment variable to populate database
ENV MYSQL_DATABASE=laravelDB
ENV MYSQL_USER=devops
ENV MYSQL_PASSWORD=rahasiaku

# Set default action when starting a container
ENTRYPOINT ["sh", "/init-db.sh"]

# Expose internal port 80 to docker host
EXPOSE 80
